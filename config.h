#ifndef CONFIG_USER_H
#define CONFIG_USER_H

#define CAPS_LOCK_STATUS

// Should fix issues with duplicate keys being emitted by mistake
#undef DEBOUNCE
#define DEBOUNCE 15

#undef AUTO_SHIFT_TIMEOUT
#define AUTO_SHIFT_TIMEOUT 140

#define NO_AUTO_SHIFT_ALPHA

#define ONESHOT_TAP_TOGGLE 3
#define ONESHOT_TIMEOUT 5000

#endif
