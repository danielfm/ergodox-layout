# Danielfm's ErgoDox Layout

This is the [QMK](https://github.com/zsa/qmk_firmware)-based layout I'm
currently using for [ErgoDox EZ](https://ergodox-ez.com), which was
heavily customized to improve comfort while using Emacs and other
applications that rely heavily on modifier keys, such as `Ctrl` and `Alt`.

## Layers

### Base Layer

It's basically a QWERTY layout with some changes designed to eliminate the
load on the pinky fingers for hitting modifier keys, resulting in reduced
tension while typing a large sequence of modified keys, which is somewhat
common in Emacs.

Some of the changes that is worth mentioning in more details:

- To avoid having to `Shift` keys frequently to type accents, such as `!`, `#`,
  and similar symbols, I enabled the QMK
  [Auto Shift](https://docs.qmk.fm/#/feature_auto_shift) feature for symbols
  and numbers. I did not enable it for alphanumeric keys since I like to be able
  to press repeated letters just by holding the key.
- `Shift`, `Ctrl` and `Alt` modifiers are mapped to the three LED lights at the
  top of the board. These keys also act as
  [one-shot modifiers](https://docs.qmk.fm/#/one_shot_keys),
  and their state is also reflected in the LEDs: the light intensity is low when
  the keys are in one-shot state, or high when locked (tap the modifiers 3 times
  to lock).
- As I spend most of the time typing inside Emacs, I moved the `Ctrl` and
  `Alt` modifiers from the [pinky](http://xahlee.info/emacs/emacs/emacs_pinky.html)
  to the thumbs. The `Space` key, when held, act as `Ctrl`, making these keys
  easily accessible from both hands, depending on the keystroke being executed;
  for instance, `C-a` and `C-e` use the right-hand Ctrl, while `C-p` and `C-n`
  use the left-hand Ctrl. I also apply this principle for other shortcuts in
  order to minimize the hand gymnastics necessary for common key combinations.
- The `Shift` key is also mirrored in both sides of the board. On the right
  side, it can be activated by long-pressing the `Enter` key. This makes it easy
  on the muscle memory when typing sequences of shifted characters with the hand
  opposite to where the character is located, in the same way as the `Ctrl`
  usage explained earlier.
- Another highly used key, `Command`, was moved to the bottom left and right
  keys of the board, which might seem counter-intuitive due to those keys being
  too far to rich, but the idea is to press those keys with my palms, instead of
  using my fingers.

### Symbols Layer

This layer is mostly the same as the default ErgoDox EZ.

### Media and Navigation Layer

This layer contains keys for media control and navigation.

## Building

To build it, you will need the [QMK](https://github.com/zsa/qmk_firmware)
firmware checked out, and this repo either checked out to something like
`keyboards/ergodox_ez/keymaps/danielfm`, or symlinked there.

One way to achieve that is this:

```
$ qmk setup -H qmk_firmware -b firmware23 zsa/qmk_firmware
$ cd qmk_firmware
$ git clone https://codeberg.org/danielfm/ergodox-layout.git \
            keyboards/ergodox_ez/keymaps/danielfm
$ qmk compile -kb ergodox_ez/glow -km danielfm
```

## License

Copyright (C) Daniel Fernandes Martins

Distributed under the New BSD License. See LICENSE for further details.
