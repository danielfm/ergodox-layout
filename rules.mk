LTO_ENABLE = yes

DEBOUNCE_TYPE = sym_defer_pk

# Ergodox EZ Glow doesn't have underlights
RGBLIGHT_ENABLE = no
RGB_MATRIX_ENABLE = yes

AUTO_SHIFT_ENABLE = yes
AUTO_SHIFT_MODIFIERS = no

TAP_DANCE_ENABLE = yes
